#Python Basics : List
#Assignment 09

#Problem Statement : Take 2 input (list's index no) from the user.
#Swap these two index no's value in list.
#Note: If user gave index no greater than list length , it should print msg like "please enter valid input"
#Eg :  a=[1,2,3,4,5]
#Input: 3  5
#Expected Result : [1,2,5,4,3]