#!/usr/bin/env python3
# Assignment 02

# Python Basics : Strings
# Problem Statement : Count the no of characters (character frequency) present in a given string.
# Input: "opstree.com"
# Expected Result : {'o': 2, 'p': 1, 's': 1, 't': 1, 'r': 1, 'e': 2, '.': 1, 'c': 1, 'm': 2}

from collections import Counter
string = input('Enter the string: ')
counts=Counter(string)
for i in string:
    print(i,counts[i])