#!/usr/bin/env python3

# Python Basics : List

# Problem Statement : Reverse a given list without using any in built function
# Input: [1, 2, 3, 4, 5]
# Expected Result : [5,4,3,2,1]