#!/usr/bin/env python3

# Write a script to swap the comma and dot in a given string.
# Input: 20.35,23
# Expected Result : 20,35.23