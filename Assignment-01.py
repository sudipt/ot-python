#!/usr/bin/env python3
# Assignment 01

# Python Basics : Strings
# Write a Python program to calculate the length of a string.
# Input: opstree
# Output: 7

string = input('Enter the string: ')
strlen = 0
for c in string:
  strlen += 1
print (strlen)