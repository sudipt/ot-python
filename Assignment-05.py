#!/usr/bin/env python3

# Write a Python program which will read a file and replace specific string with other string (case insensitive)
# input: Hello  Hi

# file before editing

# Hello This is Opstree
# hello How r u
# heLLO

# file after editing

# Hi This is Opstree
# Hi How r u
# Hi