#!/usr/bin/env python3

#Python Basics : List

# Write a programm to Calculate the sum of all the integers present in a given list.
# Input: [2, 3, "Rajat", 23, "Opstree"]
# Expected Result : 28